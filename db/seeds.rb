# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Contact.create(email: 'ian.d.desjardins@gmail.com', phone: '2486725652')
Contact.create(email: 'rideboard2808@gmail.com', phone: '2344322399')
Contact.create(email: 'john.butts@gmail.com', phone: '8296625652')
Contact.create(email: 'ian.d.desjardins@live.com', phone: '9098762623')
Contact.create(email: 'jimmy.eaton@comcast.net', phone: '73782293877', primary: true)