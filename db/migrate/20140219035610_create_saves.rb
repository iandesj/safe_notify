class CreateSaves < ActiveRecord::Migration
  def change
    create_table :saves do |t|
      t.string :safe_key

      t.timestamps
    end
  end
end
